#include <SFML/Graphics.hpp>
#include <iostream>


int main()
{
	sf::RenderWindow window(sf::VideoMode(200, 200), "jiggy", sf::Style::Close | sf::Style::Resize);
	sf::RectangleShape player(sf::Vector2f(200.0f, 200.0f));
	player.setPosition(0.0f, 0.0f);
	sf::Texture kirbyruby;
	kirbyruby.loadFromFile("ruby.jpg");
	player.setTexture(&kirbyruby);


	while (window.isOpen())
	{
		sf::Event evnt;
		while (window.pollEvent(evnt))
		{
			switch (evnt.type)
			{
			case sf::Event::Closed:
				window.close();
				break;
			}

		}
				window.clear();
				window.draw(player);
				window.display();

			}

			return 0;
		}
	
